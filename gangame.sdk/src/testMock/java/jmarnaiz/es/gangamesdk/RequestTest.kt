package jmarnaiz.es.gangamesdk

import com.google.gson.JsonObject
import com.google.gson.JsonParser
import org.junit.Assert
import org.junit.Test

class RequestTest {

    @Test
    fun dealsRequest_success() {
        val apiService = GangameApiService()
        val response = apiService.apiClient.getDeals().execute()
        val deals = response.body()

        val parser = JsonParser()
        val jsonResponse = parser.parse(MockResponses.DEALS_RESPONSE).asJsonArray

        Assert.assertTrue(response.isSuccessful)

//        if (deals != null)
//            Assert.assertEquals(deals.size, 4) // Debe devolver 4 elementos
        // Una forma alternativa en Kotlin de escribir lo de arriba es

        deals?.let {
            Assert.assertEquals(deals.size, jsonResponse.size())

            // Es una manera de "unir" ambos y recorrerlos
            deals.zip(jsonResponse).forEach {
                (deal, jsonDeal) ->
                with(jsonDeal.asJsonObject) {
                    // Lo guapo de esto es que con with, creo un contexto a esa variable, es decir, con this accedo a jsonDeal.asJsonObject
                    Assert.assertEquals(deal.title, this["title"].asString)
                    Assert.assertEquals(deal.metacriticScore, this["metacriticScore"].asInt)
                    Assert.assertEquals(deal.steamRating, this["steamRatingPercent"].asInt)
                    Assert.assertEquals(deal.thumb, this["thumb"].asString)
                }
            }
        }
    }

    @Test
    fun topRatedRequest_success() {
        val apiService = GangameApiService()
        val response = apiService.apiClient.getTopRatedGames().execute()
        val games = response.body()

        // Ejemplito de Pattern matching
        val (title, _, rating, owners, price, thumb) = TopGame("Pokemon", "", 0,0,0F,"")
        // De esta forma, title sería Pokemon, el publisher no lo guardaría, etc

        val parser = JsonParser()
        val jsonResponse: List<JsonObject> = parser.parse(MockResponses.TOP_100_GAMES)
                .asJsonObject
                .entrySet()
                .map { (_ , json) ->
                    json.asJsonObject
                }

        Assert.assertTrue(response.isSuccessful)

//        if (deals != null)
//            Assert.assertEquals(deals.size, 4) // Debe devolver 4 elementos
        // Una forma alternativa en Kotlin de escribir lo de arriba es

        games?.let {
            Assert.assertEquals(games.size, jsonResponse.size)

            // Es una manera de "unir" ambos y recorrerlos
            games.zip(jsonResponse).forEach {
                (game, jsonGames) ->
                with(jsonGames.asJsonObject) {
                    Assert.assertEquals(game.title, this["name"].asString)
                    Assert.assertEquals(game.steamRating, this["score_rank"].asInt)
                    Assert.assertEquals(game.publisher, this["publisher"].asString)
                    Assert.assertEquals(game.owners, this["owners"].asInt)
                    Assert.assertEquals(game.thumb, "http://cdn.akamai.steamstatic.com/steam/apps/${this["appid"].asInt}/capsule_184x69.jpg")
                }
            }
        }
    }
}