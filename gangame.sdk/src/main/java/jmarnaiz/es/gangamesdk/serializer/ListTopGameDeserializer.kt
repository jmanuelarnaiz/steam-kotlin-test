package jmarnaiz.es.gangamesdk.serializer

import com.google.gson.GsonBuilder
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import jmarnaiz.es.gangamesdk.TopGame
import java.lang.reflect.Type

class ListTopGameDeserializer : JsonDeserializer<ArrayList<TopGame>> {
    // La estructura inicial son pares clave - JSON. Lo que buscamos es crear un array de JSON
    override fun deserialize(json: JsonElement, typeOfT: Type?, context: JsonDeserializationContext?): ArrayList<TopGame> {
        val jsonTopGames =
                json.asJsonObject
                        .entrySet()
                        .map { (_, json) ->
                            json.asJsonObject
                        }

        // El método map() crea un nuevo array con los resultados de la llamada a la función indicada aplicados a cada uno de sus elementos.

        val gson = GsonBuilder()
                .registerTypeAdapter(TopGame::class.java, TopGameDeserializer())
                .create()


        val listTopGames: List<TopGame> = jsonTopGames.map { jsonTopGame ->
            gson.fromJson(jsonTopGame, TopGame::class.java)
        }

        val arrayListTopGames: ArrayList<TopGame> = arrayListOf() // Así se crear una lista vacía
        arrayListTopGames.addAll(listTopGames)

        return arrayListTopGames
    }
}