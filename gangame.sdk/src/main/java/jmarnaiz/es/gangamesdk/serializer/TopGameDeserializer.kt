package jmarnaiz.es.gangamesdk.serializer

import com.google.gson.Gson
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import jmarnaiz.es.gangamesdk.TopGame
import java.lang.reflect.Type

class TopGameDeserializer: JsonDeserializer<TopGame> {

    companion object { // Para declararlo estático
        const val BASE_IMG_URL = "http://cdn.akamai.steamstatic.com/steam/apps/%s/capsule_184x69.jpg"
    }
    override fun deserialize(json: JsonElement, typeOfT: Type?, context: JsonDeserializationContext?): TopGame {
        val gson = Gson()
        val topGame = gson.fromJson(json, TopGame::class.java)

        val jsonGame = json.asJsonObject
        val appId = jsonGame["appid"]

        val rawRating = jsonGame["score_rank"].asString
        val steamRating = if (rawRating.isEmpty()) 0 else Integer.parseInt(rawRating)
        val thumb = String.format(BASE_IMG_URL, appId)

        topGame.steamRating
        topGame.thumb = thumb

        return topGame
    }
}