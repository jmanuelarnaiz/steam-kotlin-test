package jmarnaiz.es.gangamesdk

import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import jmarnaiz.es.gangamesdk.serializer.ListTopGameDeserializer
import jmarnaiz.es.gangamesdk.serializer.TopGameDeserializer
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class GangameApiService(val apiConfig: GangameApiConfig = GangameClientConfig()) {

    val apiClient: RetrofitGangameAPI

    init {
        val tokenType = object : TypeToken<ArrayList<TopGame>>(){}.type
        val gson = GsonBuilder()
                .registerTypeAdapter(TopGame::class.java, TopGameDeserializer())
                .registerTypeAdapter(tokenType, ListTopGameDeserializer())
                .create()

//        val okHttpClient = OkHttpClient.Builder()
//                .addInterceptor(MockResponseInterceptor())

        val apiClientConfig =
                Retrofit.Builder()
                        .baseUrl(Routes.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create(gson))
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        //.build()

        apiConfig.setupConfig(apiClientConfig)

        apiClient = apiClientConfig.build().create(RetrofitGangameAPI::class.java) // Con esto obtenemos la clase

    }
}