package jmarnaiz.es.gangame.deals

import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.View
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import jmarnaiz.es.commons.BR
import jmarnaiz.es.commons.BaseFragment
import jmarnaiz.es.commons.BaseListFragment
import jmarnaiz.es.commons.DataBindingRecyclerAdapter
import jmarnaiz.es.gangame.Deal
import jmarnaiz.es.gangame.R
import jmarnaiz.es.gangame.data.GangameDataSource

class DealsFragment : BaseListFragment() {

    override fun getAdapter(): RecyclerView.Adapter<*> {
        return DataBindingRecyclerAdapter<Deal>(BR.deal, R.layout.item_deal)
        // Se puede acceder a través de la variable BR
    }

    override fun onResume() {
        super.onResume()
        showDeals()
    }

    private fun showDeals() {
        GangameDataSource
                .getDeals()
                .subscribe( { list ->
                    replaceItems(list)
                }, { error ->
                    showError(error)
                })
//                .subscribe(object :Observer<ArrayList<Deal>>{
//                    override fun onComplete() {
//                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//                    }
//
//                    override fun onSubscribe(d: Disposable) {
//                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//                    }
//
//                    override fun onNext(t: ArrayList<Deal>) {
//                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//                    }
//
//                    override fun onError(e: Throwable) {
//                        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
//                    }
//
//                })
    }

    private fun showError(error: Throwable) {
        error.printStackTrace()
    }

    private fun replaceItems(list: List<Deal>) {
        with (listAdapter as DataBindingRecyclerAdapter<Deal>) {
            items.clear()
            items.addAll(list)
            notifyDataSetChanged()
        }
    }

//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        super.onViewCreated(view, savedInstanceState)
//        (listAdapter as DataBindingRecyclerAdapter<Deal>).items.addAll(getDummyDeals())
//        listAdapter.notifyDataSetChanged()
//    }

//    override fun getLayoutResId(): Int {
//        return R.layout.fragment_deals
//    }

//    fun getDummyDeals(): ArrayList<Deal> {
//        return arrayListOf(Deal(
//                "Counter Strike",
//                0.99F,
//                9.99F,
//                80,
//                80,
//                "http://cdn.akamai.steamstatic.com/steam/apps/10/capsule_184x69.jpg"))
//    }
}