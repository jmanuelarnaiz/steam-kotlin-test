package jmarnaiz.es.gangame.owned

import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.View
import jmarnaiz.es.commons.BR
import jmarnaiz.es.commons.BaseListFragment
import jmarnaiz.es.commons.DataBindingRecyclerAdapter
import jmarnaiz.es.gangame.R
import jmarnaiz.es.gangame.TopGame
import jmarnaiz.es.gangame.data.GangameDataSource

class MostOwnedFragment : BaseListFragment() {

    override fun getAdapter(): RecyclerView.Adapter<*> {
        return DataBindingRecyclerAdapter<TopGame>(BR.topGame, R.layout.item_top_game)
    }

//    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
//        super.onViewCreated(view, savedInstanceState)
//        (listAdapter as DataBindingRecyclerAdapter<TopGame>).items.addAll(getDummyTopGame())
//        listAdapter.notifyDataSetChanged()
//    }
//
//    private fun getDummyTopGame(): Collection<TopGame> {
//        return arrayListOf(TopGame(
//                "Counter Strike",
//                288288,
//                80,
//                "Vale",
//                9.99F,
//                1,
//                "http://cdn.akamai.steamstatic.com/steam/apps/10/capsule_184x69.jpg"
//        ))
//    }

    override fun onResume() {
        super.onResume()
        showMostOwned()
    }

    private fun showMostOwned() {
        GangameDataSource
                .getMostOwned()
                .subscribe( { list ->
                    replaceItems(list)
                }, { error ->
                    showError(error)
                })
    }

    private fun showError(error: Throwable) {
        error.printStackTrace()
    }

    private fun replaceItems(list: List<TopGame>) {
        with (listAdapter as DataBindingRecyclerAdapter<TopGame>) {
            items.clear()
            items.addAll(list)
            notifyDataSetChanged()
        }
    }
}