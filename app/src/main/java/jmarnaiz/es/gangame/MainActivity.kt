package jmarnaiz.es.gangame

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import jmarnaiz.es.gangame.deals.DealsFragment
import jmarnaiz.es.gangame.owned.MostOwnedFragment
import jmarnaiz.es.gangame.rated.TopRatedFragment
import kotlinx.android.synthetic.main.activity_main.*

// Esto es como un ButterKnife

class MainActivity : AppCompatActivity() {

    // Para establecer un getter
//    val defaultOption: Int
//    get() = R.id.action_deals

    // Para declarar de forma estática

    companion object {

        const val DEFAULT_OPTION = R.id.action_deals

    }

    val fragments: HashMap<Int, Fragment> = hashMapOf(
            Pair(R.id.action_deals, DealsFragment()),
            Pair(R.id.action_most_owned, MostOwnedFragment()),
            Pair(R.id.action_top_rated, TopRatedFragment())
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initView()

        navigationView.selectedItemId = DEFAULT_OPTION
        navigationView.setOnNavigationItemSelectedListener {
            val fragment: Fragment? = fragments[it.itemId]

            if (fragment != null) {
                replaceFragment(fragment)
            }

            true
        }
    }

    private fun replaceFragment(fragment: Fragment?) {
        supportFragmentManager
                .beginTransaction()
                .replace(R.id.fragmentContainer, fragment)
                .commit()
    }

    private fun initView() {
        val currentFragment = supportFragmentManager.findFragmentById(R.id.fragmentContainer)

        if (currentFragment == null) {
            replaceFragment(fragments[DEFAULT_OPTION])
        }
    }
}
