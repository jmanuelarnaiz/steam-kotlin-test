package jmarnaiz.es.gangame.data

import jmarnaiz.es.gangame.Deal

object DealMapper {

    fun fromSdk(deal : jmarnaiz.es.gangamesdk.Deal) : Deal {
        return Deal(
                deal.title,
                deal.salePrice,
                deal.normalPrice,
                deal.metacriticScore,
                deal.steamRating,
                deal.thumb)
    }
}