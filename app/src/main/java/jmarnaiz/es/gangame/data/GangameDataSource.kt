package jmarnaiz.es.gangame.data

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import jmarnaiz.es.gangame.Deal
import jmarnaiz.es.gangame.TopGame
import jmarnaiz.es.gangamesdk.GangameApiService

object GangameDataSource {
    private val apiService = GangameApiService()

    fun getDeals(): Observable<ArrayList<Deal>> {
        return apiService.apiClient
                .getDealsObservable()
                .map { listDeal ->
                    // Mapeamos cada respuesta que recibimos
                    val deals = listDeal.map {
                        deal -> DealMapper.fromSdk(deal)
                    }
                    val arrayList = arrayListOf<Deal>()
                    arrayList.addAll(deals)
                    arrayList
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())

    }

    fun getTopGames(): Observable<ArrayList<TopGame>> {
        return apiService.apiClient
                .getTopRatedGamesObservable()
                .map { listGames ->
                    // Mapeamos cada respuesta que recibimos
                    val games = listGames.mapIndexed { index, game ->
                        TopGameMapper.fromSdk(game, index + 1)
                    }

                    val arrayList = arrayListOf<TopGame>()
                    arrayList.addAll(games)
                    arrayList
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())

    }

    fun getMostOwned(): Observable<ArrayList<TopGame>> {
        return apiService.apiClient
                .getMostOwnedGamesObservable()
                .map { listGames ->
                    // Mapeamos cada respuesta que recibimos
                    val games = listGames.mapIndexed { index, game ->
                        TopGameMapper.fromSdk(game, index + 1)
                    }

                    val arrayList = arrayListOf<TopGame>()
                    arrayList.addAll(games)
                    arrayList
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())

    }

}