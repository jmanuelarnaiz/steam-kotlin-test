package jmarnaiz.es.gangame.data

import jmarnaiz.es.gangame.TopGame


object TopGameMapper {

    fun fromSdk(topGame: jmarnaiz.es.gangamesdk.TopGame, position: Int) : TopGame {
        // position es la posición del juego
        return TopGame(
                topGame.title,
                topGame.owners,
                topGame.steamRating,
                topGame.publisher,
                topGame.price,
                position,
                topGame.thumb)
    }
}