package jmarnaiz.es.commons

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

abstract class BaseFragment : Fragment(){

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflate(getLayoutResId())
        // return inflater.inflate(getLayoutResId(), container, false)
    }

    abstract fun getLayoutResId(): Int
}

